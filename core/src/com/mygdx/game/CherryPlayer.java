/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import java.net.Socket;

/**
 *
 * @author alex
 */
public class CherryPlayer {
    Socket socket;
    int score=10;

    public void setScore(int score) {
        this.score = score;
    }

    public Socket getSocket() {
        return socket;
    }

    public int getScore() {
        return score;
    }

    public CherryPlayer(Socket socket) {
        this.socket = socket;
    }

    int sumaScore() {
         return score +=50;
    }
}
