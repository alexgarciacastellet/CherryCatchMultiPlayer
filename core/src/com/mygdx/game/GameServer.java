/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alex
 */
class GameServer {

    Sprite player;
    String image;
    MyGdxGame game;
    static Socket s;
    static DataInputStream din;
    static DataOutputStream dout;
    private boolean connectat;

//    public void connect(Sprite _player, String _image) {
    public void connect(MyGdxGame _game) {
        game = _game;
        player = _game.player;
        image = _game.image;

        while (!connectat) {
            try {
                System.out.println("\nIntentant connexió....");
                s = new Socket("localhost", 1978);
                connectat = true;
                System.out.println("\nConnectat!!" + s.getPort());
                din = new DataInputStream(s.getInputStream());
                dout = new DataOutputStream(s.getOutputStream());

            } catch (Exception ex) {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException ex1) {
                    Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        }

        (new Thread() {
            public void run() {

                String msgin = "";
                try {
                    while ((msgin = din.readUTF()) != null) {
                        String[] records = msgin.split("#");

                        for (int i = 0; i < records.length; i++) {

                            System.out.println(records[i]);
                            System.out.println("REBUT:: " + msgin);

                            String parts[] = new String[5];
                            parts = records[i].split(":");
                            if (parts[1].equals(game.image)) {
                               // continue;
                            }

                            if (parts[0].equals("move")) {
                                game.moveEnemy(parts[1], new Float(parts[2]), new Float(parts[3]), new Integer(parts[4]));
                            }
                            //////////game.
                        }

                    }
                } catch (IOException ex) {
                    Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();

        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void move() {
        String str = "move:" + player.hashCode() + ":" + image + ":" + player.getX() + ":" + player.getY();
        try {
            dout.writeUTF(str);
        } catch (IOException ex) {
            Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void disConnect() {
        System.out.println("DISCONNECT");
        //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void cherryCatched() {
        String str = "cherry:" + player.hashCode() + ":" + image + ":" + player.getX() + ":" + player.getY();
        try {
            dout.writeUTF(str);
        } catch (IOException ex) {
            Logger.getLogger(GameServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
