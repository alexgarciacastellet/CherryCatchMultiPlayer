package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.scene.text.Font.font;

public class MyGdxGame extends ApplicationAdapter {
    GameServer server;
    String image = "darth.png";
    int cherryDirection = 0;
    int stage=0;

    SpriteBatch batch;
    Texture img;

    public Sprite cherry;
    public Sprite player;
    float playerSpeed = 50.0f; // 10 pixels per second.
    float playerX;
    float playerY;

    Map<String, Sprite> sprites = new HashMap<String, Sprite>();
    Map<String, Integer> scores = new HashMap<String, Integer>();
    String[] icons = {"lion.png", "monster.png", "carbassa.png", "fox.png", "panda.png","ufo.png", "sim.png", "darth.png", "cherry.png", "strawberry.png"};
    
    private BitmapFont font;
    private boolean dirty = false;

    @Override
    public void create() {
        batch = new SpriteBatch();
        Random rand = new Random();
        image = icons[rand.nextInt(7)];
        
        class Task implements Runnable { public void run() {new chat_server().init(); }}
ExecutorService executor = Executors.newSingleThreadExecutor();
executor.submit(new Task());
                
                
        /*
             URI   uri=null;
        try {
            String urlString = "localhost/test/cherry/cherry.jpg";
            uri = (new URL("http://" + urlString.substring("file:".length()))).toURI();
        } catch (URISyntaxException ex) {
            Logger.getLogger(MyGdxGame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(MyGdxGame.class.getName()).log(Level.SEVERE, null, ex);
        }
       File file = new File(uri);
 */
        for (int i=0;i<10;i++){
             Sprite enemic = new Sprite(new Texture(Gdx.files.internal(icons[i])));
             sprites.put(icons[i], enemic);
             scores.put(icons[i],0);
             enemic.setPosition(-9999, -9999);
        }

        player = sprites.get(image);
        player.setPosition(playerX=rand.nextInt(500), playerY=rand.nextInt(500));
        
        cherry = sprites.get("cherry.png");
        cherry.setPosition(-9999, -9999);

        server = new GameServer();
        server.connect(this);
        server.move();
        
                font = new BitmapFont();
        font.setColor(Color.RED);
 
    }

    @Override
    public void render() {
        dirty = false;
      Gdx.gl.glClearColor(1, 55, 55, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mouMario();
        if (dirty) {
            server.move();
            player.setPosition(playerX, playerY);
        }

        batch.begin();
        //cherry.draw(batch);
        //player.draw(batch);
        //font.draw(batch, Integer.toString(scores.get(image)), playerX, playerY - 5);
        String ico=image;
        Sprite sp;
        for (Map.Entry<String, Sprite> entry : sprites.entrySet()) {
            ico = entry.getKey();
            sp = entry.getValue();
            if (sp.getX()!=-9999) sp.draw(batch);
            if (!ico.equals("cherry.png")) font.draw(batch, Integer.toString(scores.get(ico)), sp.getX(), sp.getY() - 5);
        }
        batch.end();
        Rectangle rectangleCherry = cherry.getBoundingRectangle();
        Rectangle jo = player.getBoundingRectangle();

        boolean isOverlaping = jo.overlaps(rectangleCherry);
        if (isOverlaping) {
            server.cherryCatched();
        } else {
        }

    }
/*
    @Override
    public void dispose() {
        batch.dispose();
        //playerTexture.dispose();
    }
*/
    private void mouMario() {
        if (Gdx.input.isKeyPressed(Keys.DPAD_LEFT)) {
            playerX -= Gdx.graphics.getDeltaTime() * playerSpeed;
            dirty = true;
        }
        if (Gdx.input.isKeyPressed(Keys.DPAD_RIGHT)) {
            playerX += Gdx.graphics.getDeltaTime() * playerSpeed;
            dirty = true;
        }
        if (Gdx.input.isKeyPressed(Keys.DPAD_UP)) {
            playerY += Gdx.graphics.getDeltaTime() * playerSpeed;
            dirty = true;
        }
        if (Gdx.input.isKeyPressed(Keys.DPAD_DOWN)) {
            playerY -= Gdx.graphics.getDeltaTime() * playerSpeed;
            dirty = true;
        }

        if (playerX < 0) {
            playerX = 600;
        }
        if (playerX > 600) {
            playerX = 0;
        }
        if (playerY < 0) {
            playerY = 500;
        }
        if (playerY > 500) {
            playerY = 0;
        }
    }

    void moveEnemy(String imatge, float x, float y, int score) {
        scores.put(imatge, score);
        if (imatge.equals(this.image)) return;
        Sprite sp = sprites.get(imatge);
        sp.setPosition(x, y);
        System.out.println(imatge + " score"+score);
        
        
    }
}
