/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author alex
 */
public class chat_server extends javax.swing.JFrame {

    private String st = "";
    private String missatge = "";
    float cherryX = 0;
    float cherryY = 0;

    Map<Integer, CherryPlayer> chat_list = new HashMap<Integer, CherryPlayer>();
    static final int PORT = 1978;

    public static void main(String args[]) {
        chat_server cs = new chat_server();
        cs.init();
    }
    private int cherryDirection = 0;
    private float cherrySpeed = 50.0f;

    ;

    public void init() {
        ServerSocket serverSocket = null;
        Socket socket = null;
        System.out.println("INICI... ");
        
        // envia periodicament
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        final Runnable task = new Runnable() {      public void run() { envia(); }     };
        int initialDelay = 0;
        int period = 100;
        executor.scheduleAtFixedRate(task, initialDelay, period, TimeUnit.MILLISECONDS);
        
        
        try {
            serverSocket = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        chat_list.put(0, new CherryPlayer(null));
        while (true) {
            try {
                socket = serverSocket.accept();
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }
            chat_list.put(socket.getPort(), new CherryPlayer(socket));
            new EchoThread2(socket, this).start();
        }
    }

    
       public void envia() {
        DataOutputStream dout;
        mouCherry();
        if (!missatge.equals("")) {
            for (Map.Entry<Integer, CherryPlayer> entry : chat_list.entrySet()) {
                Socket v = entry.getValue().getSocket();
                if (v==null) continue;
                try {
                    dout = new DataOutputStream(v.getOutputStream());
                    dout.writeUTF(missatge);
                } catch (IOException ex) {
                    //chat_list.remove(entry.getKey());
                    Logger.getLogger(chat_server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        missatge = "";
       
       
       }
       
 
    public void comunica(int port, String verb, String img, float x, float y) {
        if (verb.equals("cherry")) {
            cherryX = -9999;
            cherryY = -9999;
            chat_list.get(port).sumaScore();
        }

        String text;
        text = verb + ":" + img + ":" + x + ":" + y + ":" + chat_list.get(port).getScore();
        missatge += text + "#";
    }

    protected void mouCherry() {
        Random rand = new Random();
        if (cherryX < -9998) {
            cherryX--;
            if (cherryX > -10040) {
                return;
            }

            cherryX = rand.nextInt(500);
            cherryY = rand.nextInt(500);
        }

        int n = rand.nextInt(70);
        if (n > 0 && n <= 8) {
            cherryDirection = n;
        }
        if (cherryDirection == 4 || cherryDirection == 5 || cherryDirection == 6) {
            cherryX -= 0.08 * cherrySpeed;
        }
        if (cherryDirection == 8 || cherryDirection == 1 || cherryDirection == 2) {
            cherryX += 0.08 * cherrySpeed;
        }
        if (cherryDirection == 2 || cherryDirection == 3 || cherryDirection == 4) {
            cherryY += 0.08 * cherrySpeed;
        }
        if (cherryDirection == 6 || cherryDirection == 7 || cherryDirection == 8) {
            cherryY -= 0.08 * cherrySpeed;
        }

        if (cherryX < 0) {
            cherryX = 500;
        }
        if (cherryX > 500) {
            cherryX = 0;
        }
        if (cherryY < 0) {
            cherryY = 500;
        }
        if (cherryY > 500) {
            cherryY = 0;
        }
        
        missatge += "move" + ":" + "cherry.png" + ":" + cherryX + ":" + cherryY + ":" + "99" + "#";

    }
}




class EchoThread2 extends Thread {

    public Socket socket;
    public chat_server papi;

    public EchoThread2(Socket clientSocket, chat_server papi) {
        this.socket = clientSocket;
        this.papi = papi;
    }

    public void run() {
        DataOutputStream out = null;
        DataInputStream in = null;

        System.out.println("conexió: " + socket.toString() + " *** " + socket.getPort());
        try {
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            return;
        }
        String line;

        while (true) {
            try {
                line = in.readUTF();
                if ((line == null) || line.equalsIgnoreCase("QUIT")) {
                    socket.close();
                    return;
                } else {
                    out.flush();
                 //   System.out.println("REBUT>>" + line);
                    String[] exploded = new String[2];
                    exploded[0] = "0";
                    exploded[1] = line;
                    if (line.contains(":")) {
                        exploded = line.split(":");
                        String verb = exploded[0];
                        String ico = exploded[2];
                        Float x = new Float(exploded[3]);
                        Float y = new Float(exploded[4]);
                        papi.comunica(socket.getPort(), verb, ico, x, y);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }
}
